import { useState } from 'react';
import './App.css';

const objForm = {email: '', password: ''};

function App() {

  const [form, setForm] = useState(objForm);

  const handleForm = (e)=> setForm({...form, [e.target.name]: e.target.value});

  const handleSubmit = (e)=>{
    e.preventDefault();
    //Realizar petición
    fetch('http://localhost:3000/user', {
      method: 'POST',
      credentials: 'include',
      headers:{
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(form)
    }).then(async (resp)=>{
      let json = await resp.json();
      console.table(json);
    }).catch(error=>console.error(error));
    
  }

  const getSession = ()=>{
    fetch('http://localhost:3000/auth', {
      method: 'GET',
      credentials: 'include',
      headers:{
        'Content-Type': 'application/json',
      }
    }).then(async (resp)=>{
      let json = await resp.json();
      console.table(json);
    }).catch(error=>{
      console.error(error);
    })
  }

  return (
    <div className="App">
      <header className="App-header">
        <form onSubmit={handleSubmit}>
          <input onChange={handleForm} name='email' type="text" placeholder="Email"/>
          <br/>
          <input onChange={handleForm} name='password' type="password" placeholder="Password"/>
          <br/>
          <button type="submit">Register</button>
        </form>
        <br/>
        <button onClick={getSession}>Obtener datos de la sesión</button>
      </header>
    </div>
  );
}

export default App;
